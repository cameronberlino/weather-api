module.exports = {
  moduleFileExtensions: ["ts", "tsx", "js", "jsx"],
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest"
  },
  testMatch: ["**/__tests__/**/*.ts?(x)", "**/?(*.)(spec|test).ts?(x)"],
  testEnvironment: "node",
  testPathIgnorePatterns: ["/node_modules/", "/dist/"]
};
