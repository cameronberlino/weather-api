import * as express from "express";
import * as dotenv from "dotenv";
import { routes } from "./routes";
import { setup, errorHandler } from "./middleware";

const cors  = require("cors");

dotenv.config();

let requiredConfig = [
  process.env.DARK_SKY_API_URL,
  process.env.DARK_SKY_API_SECRET,
  process.env.PORT
];

for (let i = 0; i < requiredConfig.length; i++) {
  if (typeof requiredConfig[i] === "undefined") {
    throw new Error("Missing required configuration");
  }
}

const app = express();

app.use(cors());
app.use(setup);
app.use("/", routes);
app.use(errorHandler);

app.listen(process.env.PORT, (error: Error) => {
  if (error) {
    console.error(error);
  }
  console.info(`Listening on port ${process.env.PORT}`);
});
