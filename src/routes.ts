import { Router } from "express";
import GetPreviousWeekWeather from "./handlers/GetPreviousWeekWeather";

export const routes = Router();

routes.get("/weather/previousweek", GetPreviousWeekWeather);
