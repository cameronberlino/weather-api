import * as AJV from "ajv";
import {
  Request as ExpressRequest,
  Response as ExpressResponse,
  NextFunction
} from "express";

export interface BaseResponse<T> {
  deliver(body: T): BaseResponse<T>;
}

export function validateSchema(schema: object): any {
  const ajv = new AJV({ allErrors: true });
  const validate = ajv.compile(schema);

  return (req: Request & any, res: Response & any, next: NextFunction) => {
    if (validate && !validate(Object.assign(req.query, req.params, req.body))) {
      return res.status(400).send({
        error: "REQUEST_DOESNT_MATCH_SCHEMA",
        errors: validate.errors
      });
    }
    next();
  };
}

export namespace Service {
  export namespace Api {
    export namespace Weather {
      export namespace PreviousWeek {
        export namespace Get {
          export class Request {
            static schema = {
              type: "object",
              properties: {
                latitude: { type: "string" },
                longitude: {
                  type: "string"
                }
              },
              required: ["latitude", "longitude"]
            };

            latitude: string = "";
            longitude: string = "";
          }

          export class Response {
            static schema = {
              type: "object",
              properties: {
                days: {
                  type: "array"
                }
              },
              required: ["days"]
            };

            days: any[] = [];
          }

          export function implement(
            func: (
              request: ExpressRequest & { data: Request },
              response: ExpressResponse & BaseResponse<Response>,
              next: NextFunction
            ) => any
          ): any[] {
            return [
              validateSchema(Request.schema),
              async (
                request: ExpressRequest & { data: Request },
                response: ExpressResponse & BaseResponse<Response>,
                next: NextFunction
              ) => {
                try {
                  await func(request, response, next);
                } catch (e) {
                  next(e);
                }
              }
            ];
          }
        }
      }
    }
  }
}
