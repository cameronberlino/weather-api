import { Request, Response, NextFunction } from "express";

export function setup(req: any, res: any, next: NextFunction) {
  req.data = Object.assign(req.query, req.params, req.body);
  res.deliver = (body: any) => res.send(body);
  next();
}

export function errorHandler(
  error: Error,
  request: Request,
  response: Response,
  next: NextFunction
) {
  if (error) {
    console.log(error);
    return response.status(500).send({});
  }

  next();
}
