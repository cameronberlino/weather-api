import { WeatherProvider } from "../Weather/WeatherProvider";
import { Location } from "../Weather/Location";
import { Service } from "../Service";

export default Service.Api.Weather.PreviousWeek.Get.implement(
  async (request, response, next) => {
    try {
      const { latitude, longitude } = request.data;

      const days = await new WeatherProvider()
        .getProvider()
        .getPreviousWeeksWeather(new Location(latitude, longitude));

      return response.deliver({
        days: [
          days[6].data,
          days[5].data,
          days[4].data,
          days[3].data,
          days[2].data,
          days[1].data,
          days[0].data
        ]
      });
    } catch (e) {
      return next(e);
    }
  }
);
