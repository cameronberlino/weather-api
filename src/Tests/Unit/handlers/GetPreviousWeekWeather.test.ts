let next: any, res: any;

beforeEach(() => {
  jest.resetModules();
  jest.resetAllMocks();

  res = {
    deliver: jest.fn()
  };

  next = jest.fn();
});

describe("GetPreviousWeekWeather", () => {
  it("should return an error", async done => {
    jest.mock("../../../Weather/WeatherProvider", () => {
      return {
        WeatherProvider: jest.fn().mockImplementation(() => {
          return {
            getProvider: jest.fn(() => {
              return {
                getPreviousWeeksWeather: jest.fn(() => {
                  return Promise.reject("Fail");
                })
              };
            })
          };
        })
      };
    });

    const GetPreviousWeekWeather = require("../../../handlers/GetPreviousWeekWeather").default[1];
    await GetPreviousWeekWeather({ data: "" }, res, next);

    expect(next).toBeCalledWith("Fail");
    done();
  });

  it("should return the previous weeks weather", async done => {
    jest.mock("../../../Weather/WeatherProvider", () => {
      return {
        WeatherProvider: jest.fn().mockImplementation(() => {
          return {
            getProvider: jest.fn(() => {
              return {
                getPreviousWeeksWeather: jest.fn(() => {
                  return Promise.resolve([
                    { data: { weather: "" } },
                    { data: { weather: "" } },
                    { data: { weather: "" } },
                    { data: { weather: "" } },
                    { data: { weather: "" } },
                    { data: { weather: "" } },
                    { data: { weather: "" } }
                  ]);
                })
              };
            })
          };
        })
      };
    });

    const GetPreviousWeekWeather = require("../../../handlers/GetPreviousWeekWeather").default[1];
    await GetPreviousWeekWeather({ data: "" }, res, next);

    expect(res.deliver).toBeCalledWith({
      days: [
        { weather: "" },
        { weather: "" },
        { weather: "" },
        { weather: "" },
        { weather: "" },
        { weather: "" },
        { weather: "" }
      ]
    });
    done();
  });
});
