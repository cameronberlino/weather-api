import { Location } from "../../../Weather/Location";

let latitude = "123";
let longitude = "456";

beforeEach(() => {
  jest.resetModules();
  jest.resetAllMocks();
});

describe("Location", () => {
  it("should have the correct latitude and longitude", done => {
    let location = new Location(latitude, longitude);

    expect(location.latitude).toEqual(latitude);
    expect(location.longitude).toEqual(longitude);

    done();
  });

  describe("toString", () => {
    it("should return the correctly formatted location string", done => {
      let location = new Location(latitude, longitude);

      expect(location.toString()).toEqual(`${latitude},${longitude}`);

      done();
    });
  });
});
