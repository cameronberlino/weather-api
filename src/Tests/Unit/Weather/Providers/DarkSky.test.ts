import { Location } from "../../../../Weather/Location";
const MockDate = require("mockdate");

process.env.DARK_SKY_API_URL = "url";
process.env.DARK_SKY_API_SECRET = "key";

beforeEach(() => {
  jest.resetModules();
  jest.resetAllMocks();

  MockDate.set(1548048071);
});

afterAll(() => {
  MockDate.reset();
});

describe("DarkSky", () => {
  describe("getPreviousWeeksWeather", () => {
    it("should return an array of observedDays", async done => {
      jest.mock("axios");
      const axios = require("axios");
      axios.mockImplementation(() => {
        expect(axios.mock.calls[0][0].url).toEqual(
          "url/key/123,123,1461648?exclude=currently,flags,minutely"
        );
        return Promise.resolve({});
      });

      const DarkSky = require("../../../../Weather/Providers/DarkSky").DarkSky;
      const darkSky = new DarkSky();
      const weeks = await darkSky.getPreviousWeeksWeather(
        new Location("123", "123")
      );
      expect(weeks).toEqual([{}, {}, {}, {}, {}, {}, {}]);
      done();
    });
  });
});
