export class Location {
  public readonly latitude: string;
  public readonly longitude: string;

  constructor(latitude: string, longitude: string) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public toString(): string {
    return `${this.latitude},${this.longitude}`;
  }
}
