import { DarkSky } from "./Providers/DarkSky";
import { WeatherInterface } from "./WeatherInterface";

export class WeatherProvider {
  private readonly provider: WeatherInterface;

  constructor() {
    this.provider = new DarkSky();
  }

  public getProvider(): WeatherInterface {
    return this.provider;
  }
}
