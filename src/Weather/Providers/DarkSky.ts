import axios from "axios";
import { WeatherInterface } from "../WeatherInterface";
import { Location } from "../Location";
import * as moment from "moment";

export class DarkSky implements WeatherInterface {
  private readonly url: string = process.env.DARK_SKY_API_URL as string;
  private readonly secretKey: string = process.env
    .DARK_SKY_API_SECRET as string;

  public getPreviousWeeksWeather(location: Location): Promise<any> {
    let observedDays = [];

    for (let i = 1; i < 8; i++) {
      observedDays.push(
        axios({
          method: "get",
          url:
            `${this.url}/` +
            `${this.secretKey}/` +
            `${location.toString()},` +
            `${moment()
              .subtract(i, "days")
              .unix()}?exclude=currently,flags,minutely`
        })
      );
    }

    return Promise.all(observedDays);
  }
}
