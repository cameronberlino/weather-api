import { Location } from "./Location";

export interface WeatherInterface {
  getPreviousWeeksWeather<T = any>(location: Location): Promise<T>;
}
