FROM node:8.10

ENV HOME=/home/app

COPY . $HOME/weather-api/

WORKDIR $HOME/weather-api/

RUN npm install && npm run build

USER root

CMD [ "npm", "run", "start" ]